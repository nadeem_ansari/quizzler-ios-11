//
//  Question.swift
//  Quizzler
//
//  Created by Nadeem Ansari on 9/25/17.

import Foundation

class Question {
    let questionText: String
    let answer:Bool
    
    init(text:String, correctAnswer:Bool) {
        questionText = text
        answer = correctAnswer
    }
}
