//
//  QuestionBank.swift
//  Quizzler
//
//  Created by Nadeem Ansari on 9/25/17.
//  Copyright © 2017 London App Brewery. All rights reserved.
//

import Foundation

class QuestionBank {
    
    var list = [Question]()
    
    init() {
        list.append(Question(text: "Valentine\'s Day is banned in Saudi Arabia.", correctAnswer: true))
        list.append(Question(text: "A slug\'s blood is green.", correctAnswer: true))
        list.append(Question(text: "Approximately one quarter of human bones are in their feet.", correctAnswer: true))
        list.append(Question(text: "The surface area of two human lungs is approximately 70 square meters.", correctAnswer: true))
        list.append(Question(text: "In West Virginia, USA, if you hit an animal by your car accidentally, you are free to take it home to eat it.", correctAnswer: true))
        list.append(Question(text: "It\'s illegal to pee in the Portugal ocean.", correctAnswer: true))
        list.append(Question(text: "You can lead a cow down the stairs but not up the stairs.", correctAnswer: false))
        list.append(Question(text: "Google was initially called \"Backrub\".", correctAnswer: true))        
    }
}
